<?php

/**
 * Implements hook_views_data().
 */

function views_field_collection_operation_views_data() {
  $data['views_field_collection_operation']['table']['group'] = t('Views field collection operation');
  $data['views_field_collection_operation']['table']['join'] = array(
    '#global' => array(),
  );

  $data['views_field_collection_operation']['vfco'] = array(
    'title' => t('Views field collection operation'),
    'help' => t('Provide handler field to retrieve data from field collection items and to make operations on these values.'),
    'field' => array(
      'handler' => 'views_field_collection_operation_handler_vfco',
      'click sortable' => TRUE,
    ),
    'sort' => array(
       'handler' => 'views_handler_sort',
     ),
    'filter' => array(
       'handler' => 'views_handler_filter_string',
     ),

  );

  return $data;
}
