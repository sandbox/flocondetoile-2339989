<?php

/**
 * @file
 * Field handler for views field collection operation.
 */

/*
 * Views field collection operation handler class.
 *
 * @ingroup views_field_handlers
 */
class views_field_collection_operation_handler_vfco extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function query() {
  }

  /**
   * {@inheritdoc}
   *
   * Provide settings for select relevant fields
   *
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['entity_type'] = array('default' => '');
    $options['entity_id'] = array('default' => '');
    $options['fc'] = array('default' => '');
    $options['fc_item'] = array('default' => '');
    $options['fc_item_type'] = array('default' => '');
    $options['vfco_op'] = array('default' => '');

    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

     // Get a list of the available fields and arguments for trigger field and token replacement.
    $options = array();
    $fields = array('trigger_field' => t('- None -'));
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $options[t('Fields')]["[$field]"] = $handler->ui_name();
      // We only use fields up to (and including) this one.
      if ($field == $this->options['id']) {
        break;
      }
      $fields[$field] = $handler->definition['title'];
    }

    // Get all field collection available
    $fields_all = field_info_field_map();
    $fc = array('trigger_fc' => t('- None -'));
    foreach ($fields_all as $key => $value) {
      if ($value['type'] == 'field_collection') {
        $fc[$key] = $key;
      }
    }

    // Get all field item of a field collection type
    $fc_item = array('trigger_fc_item' => t('- None -'));
    foreach ($fields_all as $key => $value) {
      // dsm($fc_item_value, '$fc_item_value');
      if (array_key_exists('field_collection_item', $value['bundles'])) {
        $fc_item[$key] = $key;
      }
    }

    // Define operations available
    $vfco_op = array(
      'sum' => 'Sum',
      'aggregate'=> 'Aggregate',
      );

    // Define entity supported
    $type = array(
      'node' => 'Node',
      'user'=> 'User',
      'taxonomy_term'=> 'Taxonomy term',
    );

    // Define field collection item supported
    $fc_item_type = array(
      'number' => 'Number',
      'text'=> 'Text',
      'taxonomy_term'=> 'Taxonomy term',
    );

    $form['entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity type'),
      '#options' => $type,
      '#default_value' => $this->options['entity_type'],
      '#description' => t('Select the type of entity hosting the field collection.'),
      '#weight' => -15,
    );

    $form['entity_id'] = array(
      '#type' => 'select',
      '#title' => t('Entity id'),
      '#options' => $fields,
      '#default_value' => $this->options['entity_id'],
      '#description' => t('Select the field which contains id of entity hosting the field collection.
        This field must contain the raw id (nid, uid, tid).'),
      '#weight' => -14,

    );

    $form['fc'] = array(
      '#type' => 'select',
      '#title' => t('Field Collection'),
      '#options' => $fc,
      '#default_value' => $this->options['fc'],
      '#description' => t('Select the field collection hosting the values on which you want made operation'),
      '#weight' => -13,
    );

    $form['fc_item'] = array(
      '#type' => 'select',
      '#title' => t('Field Collection item'),
      '#options' => $fc_item,
      '#default_value' => $this->options['fc_item'],
      '#description' => t('Select the field collection item on which you want operate'),
      '#weight' => -12,
    );

    $form['fc_item_type'] = array(
      '#type' => 'select',
      '#title' => t('Field Collection item Type'),
      '#options' => $fc_item_type,
      '#default_value' => $this->options['fc_item_type'],
      '#description' => t('Select the type of field collection item on which you want operate'),
      '#weight' => -12,
    );

    $form['vfco_op'] = array(
      '#type' => 'select',
      '#title' => t('Operation'),
      '#options' => $vfco_op,
      '#default_value' => $this->options['vfco_op'],
      '#description' => t('Select the operation you want perform on field collection item value'),
      '#weight' => -11,
    );



    // $form['action_nid_pattern'] = array(
    //   '#type' => 'textfield',
    //   '#title' => t('Action Nid with pattern'),
    //   '#default_value' => $this->options['action_nid_pattern'],
    //   '#description' => t('Field which contains Action Nid. You may use replacement patterns below.'),
    //   '#weight' => -10,
    // );

    //  $form['patterns'] = array(
    //   '#type' => 'fieldset',
    //   '#title' => t('Replacement patterns'),
    //   '#collapsible' => TRUE,
    //   '#collapsed' => TRUE,
    //   '#value' => $patterns,
    // );

  }


  /**
   * Render callback handler.
   *
   * Return the markup that will appear in the rendered field.
   */
  function render($values) {

    if ($this->options['entity_id'] != 'trigger_field' &&
        $this->options['fc'] != 'trigger_fc' &&
        $this->options['fc_item'] != 'trigger_fc_item') {

      $tokens = $this->get_render_tokens($this->options['alter']);
      $entity_type = $this->options['entity_type'];
      $id = $tokens["[{$this->options['entity_id']}]"];


      $entity = reset(entity_load($entity_type, array($id)));

      // Field collection is not implement in the bundle so we do nothing
      if (!isset($entity->{$this->options['fc']})) {
        return FALSE;
      }

      // Get items form field collection
      $fc_wrapper = entity_metadata_wrapper($entity_type, $entity)->{$this->options['fc']}->value();

      // No items in field collection
      if (empty($fc_wrapper)) {
        return FALSE;
      }

      // Operation Sum
      if ($this->options['vfco_op'] == 'sum') {
        $value = 0;
        foreach ($fc_wrapper as $fc_wrapper_value) {
          if (isset($fc_wrapper_value->{$this->options['fc_item']})) {
            $collection = entity_metadata_wrapper('field_collection_item', $fc_wrapper_value);
            switch ($this->options['fc_item_type']) {
              case 'number':
                if (is_numeric($collection->{$this->options['fc_item']}->value())) {
                  $value += $collection->{$this->options['fc_item']}->value();
                }
                break;
              case 'text':
                return FALSE;
                break;
              case 'taxonomy_term':
                return FALSE;
                break;
            }

          }
        }
        return empty($value) ? FALSE : $value;
      }

      // Operation Aggregate
      if ($this->options['vfco_op'] == 'aggregate') {
        $value = array();
        foreach ($fc_wrapper as $fc_wrapper_value) {
          if (isset($fc_wrapper_value->{$this->options['fc_item']})) {
            $collection = entity_metadata_wrapper('field_collection_item', $fc_wrapper_value);
            switch ($this->options['fc_item_type']) {
              case 'number':
                if (is_numeric($collection->{$this->options['fc_item']}->value())) {
                  $value[] = $collection->{$this->options['fc_item']}->value();
                }
                break;
              case 'text':
                if (is_string($collection->{$this->options['fc_item']}->value())) {
                  $value[] = $collection->{$this->options['fc_item']}->value();
                }
                break;
              case 'taxonomy_term':
                if (is_object($collection->{$this->options['fc_item']}->value())) {
                  $value[] = $collection->{$this->options['fc_item']}->name->value();
                }
                break;
            }

          }
        }
        return empty($value) ? FALSE : implode(', ', $value);
      }



    }
    return FALSE;
  }
}
